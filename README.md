
# VIM Zen mode

A simple plugin that centers the current buffer in the middle of the
screen and hides most visible editor elements on `:ZEN`.

Runnig `:ZEN` again switches back to normal mode.

## Screenshot:

Before:

![Before](./doc/before.png)

After:

![After](./doc/after.png)

## Installation:

Using vim pack:

```bash
cd ~/.vim/pack
mkdir -p my-plugins/start
cd my-plugins/start
git clone https://codeberg.org/turysaz/vim-zenmode.git
```

