" File:         Zen mode Vim Plugin
" Maintainer:   Turysaz (turysaz@posteo.org)
" Description:  This plugin provides the :ZEN command that hides
"               most GUI elements and centers the current window.
"               This works best in a fullscreen terminal window.
" Version:      0.1

if exists("g:loaded_zenmode")
    finish
endif
let g:loaded_zenmode = 1

let s:isActive = 0

function s:GetBorderWidth()
    let l:wwidth = winwidth(winnr())

    " Do not activate any borders if the window is too narrow.
    if l:wwidth < 100
        return 0
    endif

    return (l:wwidth - 80 - 8) / 2 " (+6 for gutter, line numbers, etc.)

endfunction

function s:ActivateZenMode()

    " Turn off the status line
    set laststatus=0

    " Set colors for the TUI elements to 0, so they become invisible.
    :highlight clear VertSplit
    :highlight VertSplit ctermfg=0
    :highlight NonText   ctermfg=0

    " Get the required width for the borders.
    let l:border = s:GetBorderWidth()
    if l:border == 0
        return
    endif

    " Spawn empty windows to the left and to the right of the current buffer.

    exec("silent leftabove " . l:border . " vsplit")
    :silent edit ~/zenmode_dummy
    setlocal nonumber
    setlocal nomodifiable
    setlocal readonly
    wincmd l " jump back to the middle window.

    exec("silent rightbelow " . l:border . " vsplit")
    :silent edit ~/zenmode_dummy
    setlocal nonumber
    setlocal nomodifiable
    setlocal readonly
    wincmd h " jump back to the middle window.
endfunction

function s:DeactivateZenMode()
    set laststatus=2
    :highlight clear VertSplit
    :highlight clear NonText
    :silent bdelete ~/zenmode_dummy
endfunction

function <SID>ToggleZenMode()
    if s:isActive == 0
        call s:ActivateZenMode()
        let s:isActive = 1

        echo 'ZEN mode activated'
    else
        call s:DeactivateZenMode()
        let s:isActive = 0

        echo 'ZEN mode deactivated'
    endif
endfunction

command ZEN call <SID>ToggleZenMode()

